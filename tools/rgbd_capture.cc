/*
 * This file is part of the OpenKinect Project. http://www.openkinect.org
 *
 * Copyright (c) 2011 individual OpenKinect contributors. See the CONTRIB file
 * for details.
 *
 * This code is licensed to you under the terms of the Apache License, version
 * 2.0, or, at your option, the terms of the GNU General Public License,
 * version 2.0. See the APACHE20 and GPL2 files for the text of the licenses,
 * or the following URLs:
 * http://www.apache.org/licenses/LICENSE-2.0
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you redistribute this file in source form, modified or unmodified, you
 * may:
 *   1) Leave this header intact and distribute it under the same terms,
 *      accompanying it with the APACHE20 and GPL20 files, or
 *   2) Delete the Apache 2.0 clause and accompany it with the GPL2 file, or
 *   3) Delete the GPL v2 clause and accompany it with the APACHE20 file
 * In all cases you must keep the copyright notice intact and include a copy
 * of the CONTRIB file.
 *
 * Binary distributions must follow the binary distribution requirements of
 * either License.
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <cstdlib>

/// [headers]
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>

#include <opencv2/opencv.hpp>
#include <glog/logging.h>

#include "System.h"

const unsigned int kDepthWidth  = 512;
const unsigned int kDepthHeight = 424;
const unsigned int kRgbWidth    = 1920;
const unsigned int kRgbHeight   = 1080;
const unsigned int kSaveWidth   = 640;
const unsigned int kSaveHeight  = 360;

const int kKeySpace  = 32;
const int kKeyEscape = 27;

typedef std::vector< std::pair<cv::Mat, unsigned int> > KinectImages;

cv::Mat ConvertDepthfToDepthS(cv::Mat& matf) {
  cv::Mat mats = cv::Mat(matf.rows, matf.cols, CV_16UC1);
  for (int i = 0; i < matf.rows; ++i) {
    for (int j = 0; j < matf.cols; ++j) {
      float d = matf.at<float>(i, j);
      if (isinf(d) || isnan(d) || d < 0) d = 0;
      unsigned short d_save = (unsigned short) (d);
      mats.at<unsigned short> (i, j) = d_save;
    }
  }
  return mats;
}

void SaveImages(KinectImages& depth_imgs, KinectImages& color_imgs,
                std::string base_path) {
  std::string cmd, color_base_path, depth_base_path;

  cmd = "ls " + base_path;
  LOG(INFO) << cmd;
  if (base_path.size() == 0 || system(cmd.c_str())) {
    LOG(ERROR) << "Invalid dir, would not attempt to save!";
    return;
  }

  depth_base_path = base_path + "/depth";
  color_base_path = base_path + "/rgb";
  cmd = "mkdir " + depth_base_path;
  if (system(cmd.c_str())) {
    LOG(ERROR) << "depth file exists, clearing them all";
    cmd = "rm " + base_path + "/depth/*.png";
    system(cmd.c_str());
  }
  cmd = "mkdir " + color_base_path;
  if (system(cmd.c_str())) {
    LOG(ERROR) << "color file exists, clearing them all";
    cmd = "rm " + base_path + "/rgb/*.jpg";
    system(cmd.c_str());
  }

  size_t size = depth_imgs.size();
  std::stringstream ss;
  for (size_t i = 0; i < size; ++i) {
    ss.str("");
    ss << depth_base_path << "/"
       << std::setfill('0') << std::setw(5) << i << "-"
       << std::setfill('0') << std::setw(8) << depth_imgs[i].second
       << ".png";
    LOG(INFO) << ss.str();
    cv::Mat depth_16u = ConvertDepthfToDepthS(depth_imgs[i].first);
    cv::imwrite(ss.str(), depth_16u);

    ss.str("");
    ss << color_base_path << "/"
       << std::setfill('0') << std::setw(5) << i << "-"
       << std::setfill('0') << std::setw(8) << color_imgs[i].second
       << ".jpg";
    LOG(INFO) << ss.str();
    cv::imwrite(ss.str(), color_imgs[i].first);
  }

  cmd = "ls " + color_base_path + " > " + base_path + "/rgb.txt";
  system(cmd.c_str());
  cmd = "ls " + depth_base_path + " > " + base_path + "/depth.txt";
  system(cmd.c_str());
}

int main(int argc, char *argv[]) {
  // argv[1]: vocab, argv[2]: config, argv[3]: path/to/save
  ORB_SLAM2::System SLAM(argv[1], argv[2],
                         ORB_SLAM2::System::RGBD, false);

  // Context configuration
  libfreenect2::Freenect2 freenect2;
  libfreenect2::Freenect2Device *dev = 0;
  libfreenect2::PacketPipeline *pipeline = 0;

  // Enumerate USB devices
  if (freenect2.enumerateDevices() == 0) {
    LOG(ERROR) << "No device connected!";
    return -1;
  }

  // Open device at the enumerated valid serial
  std::string serial = freenect2.getDefaultDeviceSerialNumber();
  pipeline = new libfreenect2::OpenGLPacketPipeline();
  if (pipeline) {
    dev = freenect2.openDevice(serial, pipeline);
  }
  if (dev == 0) {
    LOG(ERROR) << "Failure opening device!";
    return -1;
  }

  // Sync data reader
  libfreenect2::SyncMultiFrameListener listener(
      libfreenect2::Frame::Color
      | libfreenect2::Frame::Ir
      | libfreenect2::Frame::Depth);
  libfreenect2::FrameMap frames;
  dev->setColorFrameListener(&listener);
  dev->setIrAndDepthFrameListener(&listener);

  // Start
  LOG(INFO) << "device serial: " << dev->getSerialNumber();
  LOG(INFO) << "device firmware: " << dev->getFirmwareVersion();
  dev->start();

  // Register Depth and Color images
  libfreenect2::Registration* registration
      = new libfreenect2::Registration(dev->getIrCameraParams(),
                                       dev->getColorCameraParams());
  libfreenect2::Frame
      undistorted_depth(kDepthWidth, kDepthHeight,   4),
      registered_rgb   (kDepthWidth, kDepthHeight,   4), // rgb -> depth
      registered_depth (kRgbWidth,   kRgbHeight + 2, 4); // depth -> rgb

  cv::Mat rgb_mat, depth_mat_raw, depth_mat;
  bool is_recording = false;

  cv::namedWindow("rgb", CV_WINDOW_OPENGL   | CV_WINDOW_AUTOSIZE);
  cv::moveWindow("rgb", 0, 0);
  cv::namedWindow("depth", CV_WINDOW_OPENGL | CV_WINDOW_AUTOSIZE);
  cv::moveWindow("depth", 0, 360);

  KinectImages rgb_imgs;
  KinectImages depth_imgs;
  while (true) {
    // 1. Read frame
    listener.waitForNewFrame(frames);
    libfreenect2::Frame *rgb   = frames[libfreenect2::Frame::Color];
    // libfreenect2::Frame *ir    = frames[libfreenect2::Frame::Ir];
    libfreenect2::Frame *depth = frames[libfreenect2::Frame::Depth];

    // 2. Register frames
    registration->apply(rgb, depth, &undistorted_depth, &registered_rgb,
                        true, &registered_depth);
    rgb_mat       = cv::Mat(kRgbHeight,     kRgbWidth, CV_8UC4,
                            rgb->data);
    depth_mat_raw = cv::Mat(kRgbHeight + 2, kRgbWidth, CV_32F,
                            registered_depth.data);
    depth_mat     = depth_mat_raw(cv::Rect(0, 1, kRgbWidth, kRgbHeight));

    // 3. Collect desired depth and color frames
    cv::resize(rgb_mat,   rgb_mat,   cv::Size(kSaveWidth, kSaveHeight));
    cv::resize(depth_mat, depth_mat, cv::Size(kSaveWidth, kSaveHeight),
               0, 0, cv::INTER_NEAREST);
    cv::flip(rgb_mat,   rgb_mat,   1);
    cv::flip(depth_mat, depth_mat, 1);

    // 4. Display
    // cv::imshow("rgb",    rgb_mat);
    cv::imshow("depth",  0.0002f * depth_mat);

    // 5. Save them if it is recording; SLAM
    if (is_recording) {
      LOG(INFO) << "timestamp: " << rgb->timestamp << " " << depth->timestamp;
      rgb_imgs.push_back(std::make_pair(rgb_mat, rgb->timestamp));
      depth_imgs.push_back(std::make_pair(depth_mat, depth->timestamp));

      double tframe;
      cv::Mat rgb_mat_slam = rgb_mat.clone();
      cv::Mat depth_mat_slam = depth_mat.clone();
      cv::Mat Tcw = SLAM.TrackRGBD(rgb_mat_slam, depth_mat_slam, tframe);
      cv::Mat draw = SLAM.mpFrameDrawer->DrawFrame();
      cv::imshow("display", draw);
    }

    listener.release(frames);
    if (int key = cv::waitKey(5)) {
      if (key == kKeyEscape) break;
      if (key == kKeySpace && ! is_recording) {
        is_recording = true;
      }
    }
  }
  dev->stop();
  dev->close();
  delete registration;

  SLAM.Shutdown();

  // Final: check, show, and save
  CHECK(rgb_imgs.size() == depth_imgs.size())
  << "Invalid RGB - Depth correspondences";
  size_t size = rgb_imgs.size();
//  for (int i = 0; i < size; ++i) {
//    cv::imshow("rgb",   rgb_imgs[i].first);
//    cv::imshow("depth", depth_imgs[i].first);
//    cv::waitKey(1);
//  }

  // Saving
  std::string base_path = (argc < 4) ? "." : std::string(argv[3]);
  SaveImages(depth_imgs, rgb_imgs, base_path);
  SLAM.SaveTrajectoryTUM(std::string(argv[3]) + "/trajectory.txt");

  return 0;
}
